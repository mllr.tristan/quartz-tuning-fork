import cv2
import os

y = 320 #300
h = 50  #180
x = 257 #200
w = 5   #300

for file in os.listdir("/home/tristan/Downloads/Quartz"):
    capture = cv2.VideoCapture(file)

    if file.endswith(".avi"):
        freq = str(file.split(".")[0])
        try: 
            os.rmdir(f"/home/tristan/Downloads/Quartz/output/{freq}")
        except: pass
        os.mkdir(f"/home/tristan/Downloads/Quartz/output/{freq}")
        
        for nb in range(50):
            success, frame = capture.read()
            b,g,r = cv2.split(frame)
            if success:
                crop_img = frame[y:y+h, x:x+w] #Cropping the frame to fit only the fork
                if nb>9:
                    cv2.imwrite(f'/home/tristan/Downloads/Quartz/output/{freq}/{freq}_{nb}.jpg', crop_img) 
                else:
                    cv2.imwrite(f'/home/tristan/Downloads/Quartz/output/{freq}/{freq}_0{nb}.jpg', crop_img)               
                print("OK")
            else:
                break
            
    print(os.path.join("/home/tristan/Downloads/Quartz", file))
 
capture.release()