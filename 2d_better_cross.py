import os
from skimage.metrics import structural_similarity
import argparse
import imutils
import cv2
import csv
import numpy as np
import matplotlib as mlp
import matplotlib.mlab
from scipy import signal



for folder in sorted(os.listdir(f"/home/tristan/Downloads/Quartz/output/")):
    freq = folder
    #print(freq)
    nbs = [0]
    scores = [0]
    xs = [0]
    ys = [0]
    
    for first_file in sorted(os.listdir(f"/home/tristan/Downloads/Quartz/output/{freq}")):
        x = 0
        for file in sorted(os.listdir(f"/home/tristan/Downloads/Quartz/output/{freq}")):
            #print(file)
            if file != first_file:
                imageA = cv2.imread(f"/home/tristan/Downloads/Quartz/output/{freq}/{first_file}")
                imageB = cv2.imread(f"/home/tristan/Downloads/Quartz/output/{freq}/{file}")

                nb = int(file.split("_")[1].split(".")[0])
                nbs.append(nb)

               
                grayA = cv2.cvtColor(imageA, cv2.COLOR_BGR2GRAY)
                grayB = cv2.cvtColor(imageB, cv2.COLOR_BGR2GRAY)

                corr = signal.correlate2d(grayA, grayB, boundary='symm', mode='full')
                y, x = np.unravel_index(np.argmax(corr), corr.shape)  # find the match
                xs.append(x)
                ys.append(y)

                print(f'{x},{y}')

                with open(f'/home/tristan/Downloads/Quartz/output/{freq}.csv', 'a') as f:
                    # create the csv writer
                    writer = csv.writer(f)

                    # write a row to the csv file
                    writer.writerow([nb,x,y])
        break
    
    with open(f'/home/tristan/Downloads/Quartz/output/extre.csv', 'a') as f:
        # create the csv writer
        writer = csv.writer(f)

        if 0 in scores:
            scores.remove(0)
        if 1 in scores:
            scores.remove(1)

        # write a row to the csv file
        writer.writerow([freq,max(xs),min(xs),max(ys),min(ys)])
        scores.clear()

        

    
    