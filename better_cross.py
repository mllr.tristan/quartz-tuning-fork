import os
from skimage.metrics import structural_similarity
import argparse
import imutils
import cv2
import csv
import numpy as np
import matplotlib as mlp
import matplotlib.mlab

chemin = "/home/tristan/Downloads/Quartz/output"


for folder in sorted(os.listdir(f"{chemin}/")):
    freq = folder
    #print(freq)
    nbs = [0]
    scores = [0]
    
    for first_file in sorted(os.listdir(f"{chemin}/{freq}")):
        x = 0
        for file in sorted(os.listdir(f"{chemin}/{freq}")):
            #print(file)
            if file != first_file:
                imageA = cv2.imread(f"{chemin}/{freq}/{first_file}")
                imageB = cv2.imread(f"{chemin}/{freq}/{file}")

                nb = int(file.split("_")[1].split(".")[0])
                nbs.append(nb)

                #Transforme img RGB en N&B, retire la moyenne, transpose la matrice et la transforme en liste
                #grayA = np.array(mlp.mlab.detrend(cv2.cvtColor(imageA, cv2.COLOR_BGR2GRAY))).T.tolist()
                #grayB = np.array(mlp.mlab.detrend(cv2.cvtColor(imageB, cv2.COLOR_BGR2GRAY))).T.tolist()
                #Python c'est magique !
                grayA = np.array(cv2.cvtColor(imageA, cv2.COLOR_BGR2GRAY)).T.tolist()
                grayB = np.array(cv2.cvtColor(imageB, cv2.COLOR_BGR2GRAY)).T.tolist()

                score = max(np.correlate(grayA[0],grayB[0], "full"))
                
                print(f'{nb},{score}')
                scores.append(score)
                nbs.append(nb)

                with open(f'{chemin}/{freq}.csv', 'a') as f:
                    # create the csv writer
                    writer = csv.writer(f)

                    # write a row to the csv file
                    writer.writerow([nb,score])
        break
    
    with open(f'{chemin}/extre.csv', 'a') as f:
        # create the csv writer
        writer = csv.writer(f)

        if 0 in scores:
            scores.remove(0)
        if 1 in scores:
            scores.remove(1)

        # write a row to the csv file
        writer.writerow([freq,max(scores)-min(scores),min(scores),max(scores)])
        scores.clear()

        

    
    